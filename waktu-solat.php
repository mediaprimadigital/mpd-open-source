<?php
 /*
   Plugin Name: Waktu Solat Malaysia
   description: Display waktu solat malaysia for today in widget size
   Version: 1.2
   Author: Engineering & Design, Media Prima Digital
   License: GPL2
   */

   global $wp;
   function waktu_solat_init()
   {
    function waktu_solat_shortcode($atts = [], $content = null)
    {  
      wp_register_script('prayer-time-async', plugin_dir_url(__FILE__) . 'prayer-time.js', array('jquery'), '1.0', true);
      wp_enqueue_script('prayer-time-async');

      

      $content = '<script src=""></script>
      <!-- Latest compiled and minified CSS -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
      <div style="width: 100%;padding: 5px;" class="hider">
      <style>
        #areaSelect {
          width: 100% !important;
          padding: 0;
        }
        #areaSelect + .select2-container {
          margin-bottom: 10px;
        }
    </style>
    <div class="today" style="width:100%;padding:5px;margin-bottom:10px;">
        1 January 2017, Thursday
    </div>
<div style="width: 100%;padding: 0px 5px;" class="form-group">
    <select class="form-control" data-live-search="true" id="areaSelect">
      <optgroup label="Wilayah Persekutuan">
        <option value="wlp-0">Kuala Lumpur</option>
        <option value="wlp-1">Labuan</option>
      </optgroup>
      <optgroup label="Johor">
        <option value="jhr-0"> Batu Pahat</option>
        <option value="jhr-1"> Gemas</option>
        <option value="jhr-2"> Johor Bahru</option>
        <option value="jhr-3"> Kluang</option>
        <option value="jhr-4"> Kota Tinggi</option>
        <option value="jhr-5"> Mersing</option>
        <option value="jhr-6"> Muar</option>
        <option value="jhr-7"> Pemanggil</option>
        <option value="jhr-8"> Pontian</option>
        <option value="jhr-9"> Pulau Aur</option>
        <option value="jhr-10"> Segamat</option>
      </optgroup>
      <optgroup label="Kedah">
        <option value="kdh-0">Baling</option>
        <option value="kdh-1">Bandar Baharu</option>
        <option value="kdh-2">Kota Setar</option>
        <option value="kdh-3">Kuala Muda</option>
        <option value="kdh-4">Kubang Pasu</option>
        <option value="kdh-5">Kulim</option>
        <option value="kdh-6">Langkawi</option>
        <option value="kdh-7">Padang Terap</option>
        <option value="kdh-8">Pendang</option>
        <option value="kdh-9">Pokok Sena  </option>
        <option value="kdh-10">Puncak Gunung Jerai  </option>
        <option value="kdh-11">Sik</option>
        <option value="kdh-12">Yan</option>
      </optgroup>
      <optgroup label="Kelantan">      
        <option value="ktn-0"> Bachok</option>
        <option value="ktn-1"> Bertam</option>
        <option value="ktn-2"> Jeli</option>
        <option value="ktn-3"> Kota Bharu </option>
        <option value="ktn-4"> Kuala Krai</option>
        <option value="ktn-5"> Machang</option>
        <option value="ktn-6"> Mukim Chiku</option>
        <option value="ktn-7"> Mukim Galas</option>
        <option value="ktn-8"> Pasir Mas</option>
        <option value="ktn-9"> Pasir Puteh</option>
        <option value="ktn-10"> Tanah Merah</option>
        <option value="ktn-11"> Tumpat</option>
      </optgroup>
      <optgroup label="Melaka">
        <option value="mlk-0"> Alor Gajah</option>
        <option value="mlk-1"> Bandar Melaka</option>
        <option value="mlk-2"> Jasin </option>
        <option value="mlk-3"> Masjid Tanah  </option>
        <option value="mlk-4"> Merlimau </option>
        <option value="mlk-5"> Nyalas </option>
      </optgroup>
      <optgroup label="Negeri Sembilan">
        <option value="ngs-0"> Jelebu</option>
        <option value="ngs-1"> Jempol</option>
        <option value="ngs-2"> Kuala Pilah</option>
        <option value="ngs-3"> Port Dickson</option>
        <option value="ngs-4"> Rembau</option>
        <option value="ngs-5"> Seremban</option>
        <option value="ngs-6"> Tampin</option>
      </optgroup>
      <optgroup label="Pahang">
        <option value="phg-0"> Bentong</option>
        <option value="phg-1"> Bera</option>
        <option value="phg-2"> Bukit Fraser</option>
        <option value="phg-3"> Cameron Highland</option>
        <option value="phg-4"> Chenor</option>
        <option value="phg-5"> Genting Highlands</option>
        <option value="phg-6"> Jerantut</option>
        <option value="phg-7"> Kuala Lipis</option>
        <option value="phg-8"> Kuantan</option>
        <option value="phg-9"> Maran</option>
        <option value="phg-10"> Muadzam Shah</option>
        <option value="phg-11"> Pekan</option>
        <option value="phg-12"> Pulau Tioman</option>
        <option value="phg-13"> Raub</option>
        <option value="phg-14"> Rompin</option>
        <option value="phg-15"> Temerloh</option>
      </optgroup>
      <optgroup label="Perak">
        <option value="prk-0"> Bagan Datoh</option>
        <option value="prk-1"> Bagan Serai</option>
        <option value="prk-2"> Batu Gajah</option>
        <option value="prk-3"> Belum</option>
        <option value="prk-4"> Beruas</option>
        <option value="prk-5"> Bukit Larut</option>
        <option value="prk-6"> Grik</option>
        <option value="prk-7"> Ipoh</option>
        <option value="prk-8"> Kampar</option>
        <option value="prk-9"> Kampung Gajah</option>
        <option value="prk-10"> Kuala Kangsar</option>
        <option value="prk-11"> Lenggong</option>
        <option value="prk-12"> Lumut</option>
        <option value="prk-13"> Parit</option>
        <option value="prk-14"> Parit Buntar</option>
        <option value="prk-15"> Pengkalan Hulu </option>
        <option value="prk-16"> Pulau Pangkor</option>
        <option value="prk-17"> Selama</option>
        <option value="prk-18"> Setiawan</option>
        <option value="prk-19"> Slim River</option>
        <option value="prk-20"> Sri Iskandar</option>
        <option value="prk-21"> Sungai Siput</option>
        <option value="prk-22"> Taiping</option>
        <option value="prk-23"> Tanjung Malim</option>
        <option value="prk-24"> Tapah</option>
        <option value="prk-25"> Teluk Intan</option>
        <option value="prk-26"> Temengor</option>
      </optgroup>
      <optgroup label="Perlis">
        <option value="pls-0"> Arau</option>
        <option value="pls-0"> Kangar</option>
        <option value="pls-0"> Padang Besar </option> 
      </optgroup>
      <optgroup label="Pulau Pinang">
        <option value="png-0"> Pulau Pinang</option>
      </optgroup>
      <optgroup label="Sabah">
        <option value="sbh-0"> Balong</option>
        <option value="sbh-1"> Bandar Bukit Garam</option>
        <option value="sbh-2"> Beaufort</option>
        <option value="sbh-3"> Beluran</option>
        <option value="sbh-4"> Gunung Kinabalu</option>
        <option value="sbh-5"> Kalabakan</option>
        <option value="sbh-6"> Keningau</option>
        <option value="sbh-7"> Kota Belud</option>
        <option value="sbh-8"> Kota Kinabalu</option>
        <option value="sbh-9"> Kota Marudu</option>
        <option value="sbh-10"> Kuala Penyu </option>
        <option value="sbh-11"> Kuamut</option>
        <option value="sbh-12"> Kudat</option>
        <option value="sbh-13"> Kunak</option>
        <option value="sbh-14"> Lahat Datu</option>
        <option value="sbh-15"> Long Pa Sia </option>
        <option value="sbh-16"> Membakut</option>
        <option value="sbh-17"> Merotai</option>
        <option value="sbh-18"> Nabawan</option>
        <option value="sbh-19"> Papar</option>
        <option value="sbh-20"> Penampang</option>
        <option value="sbh-21"> Pensiangan</option>
        <option value="sbh-22"> Pinangah</option>
        <option value="sbh-23"> Pitas</option>
        <option value="sbh-24"> Pulau Banggi</option>
        <option value="sbh-25"> Ranau</option>
        <option value="sbh-26"> Sahabat</option>
        <option value="sbh-27"> Sandakan</option>
        <option value="sbh-28"> Semawang</option>
        <option value="sbh-29"> Semporna</option>
        <option value="sbh-30"> Silabukan</option>
        <option value="sbh-31"> Sipitang</option>
        <option value="sbh-32"> Tambisan</option>
        <option value="sbh-33"> Tambunan</option>
        <option value="sbh-34"> Tawau</option>
        <option value="sbh-35"> Telupit</option>
        <option value="sbh-36"> Temanggong</option>
        <option value="sbh-37"> Tenom</option>
        <option value="sbh-38"> Terusan</option>
        <option value="sbh-39"> Tuaran</option>
        <option value="sbh-40"> Tungku</option>
        <option value="sbh-41"> Weston</option>
      </optgroup>
      <optgroup label="Sarawak">
        <option value="swk-0"> Bau</option>
        <option value="swk-1"> Bekenu</option>
        <option value="swk-2"> Belaga</option>
        <option value="swk-3"> Belawai</option>
        <option value="swk-4"> Belingan</option>
        <option value="swk-5"> Betong</option>
        <option value="swk-6"> Bintulu</option>
        <option value="swk-7"> Bitangor</option>
        <option value="swk-8"> Dalat</option>
        <option value="swk-9"> Daro</option>
        <option value="swk-10"> Debak</option>
        <option value="swk-11"> Engkelili</option>
        <option value="swk-12"> Igan</option>
        <option value="swk-13"> Julau</option>
        <option value="swk-14"> Kabong</option>
        <option value="swk-15"> Kanowit</option>
        <option value="swk-16"> Kapit</option>
        <option value="swk-17"> Kuching</option>
        <option value="swk-18"> Lawas</option>
        <option value="swk-19"> Limbang</option>
        <option value="swk-20"> Lingga</option>
        <option value="swk-21"> Lundu</option>
        <option value="swk-22"> Marudi</option>
        <option value="swk-23"> Matu</option>
        <option value="swk-24"> Meludam</option>
        <option value="swk-25"> Miri</option>
        <option value="swk-26"> Niah</option>
        <option value="swk-27"> Oya</option>
        <option value="swk-28"> Pusa</option>
        <option value="swk-29"> Rajang</option>
        <option value="swk-30"> Roban</option>
        <option value="swk-31"> Samarahan</option>
        <option value="swk-32"> Saratok</option>
        <option value="swk-33"> Sarikei</option>
        <option value="swk-34"> Sebauh</option>
        <option value="swk-35"> Sebuyau</option>
        <option value="swk-36"> Sematan</option>
        <option value="swk-37"> Serian</option>
        <option value="swk-38"> Sibu</option>
        <option value="swk-39"> Sibuti</option>
        <option value="swk-40"> Simunjan</option>
        <option value="swk-41"> Song</option>
        <option value="swk-42"> Spaoh</option>
        <option value="swk-43"> Sri Aman</option>
        <option value="swk-44"> Sundar</option>
        <option value="swk-45"> Tatau</option>
        <option value="swk-46"> Terusan</option>
      </optgroup>
      <optgroup label="Selangor">
        <option value="sgr-0"> Gombak</option>
        <option value="sgr-1"> Hulu Langat</option>
        <option value="sgr-2"> Hulu Selangor</option>
        <option value="sgr-3"> Klang</option>
        <option value="sgr-4"> Kuala Langat</option>
        <option value="sgr-5"> Kuala Selangor</option>
        <option value="sgr-6"> Petaling</option>
        <option value="sgr-7"> Rawang</option>
        <option value="sgr-8"> Sabak Bernam </option>
        <option value="sgr-9"> Sepang</option>
        <option value="sgr-10"> Shah Alam</option>
      </optgroup>
      <optgroup label="Terengganu">
        <option value="trg-0"> Besut</option>
        <option value="trg-1"> Hulu Terrenganu  </option>
        <option value="trg-2"> Kemaman Dungun</option>
        <option value="trg-3"> Kuala Terengganu</option>
        <option value="trg-4"> Marang</option>
        <option value="trg-5"> Setiu</option>
      </optgroup>
    </select>
</div>
<div class="today_kl_prayer" style="width: 100%;padding: 0px 5px;position:relative;">
    <div style="width: 100%;display: inline-block;">
        <div style="width: 50%;float: left;padding-left: 5px;margin: 5px 0px;">Subuh</div>
        <div style="width: 20%;float: right;overflow: auto;text-align: right;" class="kl_time">00:00am</div>
    </div>
    <div style="width: 100%;display: inline-block;">
        <div style="width: 50%;float: left;padding-left: 5px;margin: 5px 0px;">Syuruk</div>
        <div style="width: 20%;float: right;overflow: auto;text-align: right;" class=" kl_time">00:00am</div>
    </div>
    <div style="width: 100%;display: inline-block;">
        <div style="width: 50%;float: left;padding-left: 5px;margin: 5px 0px;">Zuhur</div>
        <div style="width: 20%;float: right;overflow: auto;text-align: right;" class=" kl_time">00:00am</div>
    </div>
    <div style="width: 100%;display: inline-block;">
        <div style="width: 50%;float: left;padding-left: 5px;margin: 5px 0px;">Asar</div>
        <div style="width: 20%;float: right;overflow: auto;text-align: right;" class=" kl_time">00:00am</div>
    </div>
    <div style="width: 100%;display: inline-block;">
        <div style="width: 50%;float: left;padding-left: 5px;margin: 5px 0px;">Maghrib</div>
        <div style="width: 20%;float: right;overflow: auto;text-align: right;" class=" kl_time">00:00am</div>
    </div>
    <div style="width: 100%;display: inline-block;">
        <div style="width: 50%;float: left;padding-left: 5px;margin: 5px 0px;">Isyak</div>
        <div style="width: 20%;float: right;overflow: auto;text-align: right;" class=" kl_time">00:00am</div>
    </div>
</div>
<div style="width:100%;padding:20px 0px;">
    <div style="width:100%; border:1px solid #f0cc61; padding:20px 0; background: #f0cc61;">
        <div style="text-align: center;">
            <p>Waktu Solat Ini Dibawakan Oleh<br/><strong>Waktu Solat Malaysia</strong></p>
        </div>
        <div style="margin: 0 auto;display: table;text-align: center;width: 100%;">
            <div style="text-align: right;width: 50%;display: table-cell;vertical-align: middle;padding-right:10px;">
                <img src="https://storage.googleapis.com/raudhah/WSM-App-Icon-2019-100px.png">
            </div>
            <div style="text-align: left;width: 50%;padding-top: 8px;display: table-cell;vertical-align: middle;">
                <div style="width:100%;">
                    <div style="width:100%;display: inline-block;">
                        <a href="https://itunes.apple.com/us/app/waktu-solat-malaysia/id1443457663?ls=1&mt=8" target="_blank">
                <img src="https://www.mediaprimalabs.com/wp-content/uploads/2016/04/img_btn_appstore.png" alt="img_btn_appstore" width="120" height="36">
              </a>
                    </div>
                    <div style="width:100%;display: inline-block;">
                        <a href="https://play.google.com/store/apps/details?id=com.murad.waktusolat&hl=en" target="_blank">
                <img src="https://www.mediaprimalabs.com/wp-content/uploads/2016/04/img_btn_playstore.png" alt="img_btn_playstore" width="120" height="36">
              </a>
                    </div>
                </div>

            </div>
            <div style="clear:both"></div>
        </div>
    </div>
</div>
</div>';
      return $content;
  }
  add_shortcode('waktu_solat', 'waktu_solat_shortcode');
}
add_action('init', 'waktu_solat_init');

function dayEngToMalay($day){
    switch($day){
      case 'Monday':
      $dayMalay = 'Isnin';
      break;
      case 'Tuesday':
      $dayMalay = 'Selasa';
      break;
      case 'Wednesday':
      $dayMalay = 'Rabu';
      break;
      case 'Thursday':
      $dayMalay = 'Khamis';
      break;
      case 'Friday':
      $dayMalay = 'Jumaat';
      break;
      case 'Saturday':
      $dayMalay = 'Sabtu';
      break;
      case 'Sunday':
      $dayMalay = 'Ahad';
      break;
      default:
      $dayMalay = '';
  }
  return $dayMalay;
}

function waktu_solat_ramadhan_init()
{
    function waktu_solat_ramadhan_shortcode($atts = [], $content = null)
    { 
      if (isset($_GET['code'])) {
        $code = $_GET['code'];
    } else {
        $code = "sgr03";
    }

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.azanpro.com/times/date.json?zone=".$code."&start=17-05-2018&end=16-06-2018&format=12-hour",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache",
          "postman-token: 2423f06b-59cf-3ec5-080c-7f66ed45af09"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        $content =  "cURL Error #:" . $err;
    } else {
        $response = json_decode($response);
        $i = 1;

        $content = '<div class="row"><div class="col-md-12"><table class="table table-striped table-responsive-md"><thead>
        <tr>
        <th scope="col">Masihi</th>
        <th scope="col">Hari</th>
        <th scope="col">Ramadhan</th>
        <th scope="col">Imsak</th>
        <th scope="col">Berbuka</th>
        </tr>
        </thead>
        <tbody class="daysPrayer">';
        foreach ($response->prayer_times as $key => $value):
          $content .= '<tr><td scope="col">'. $value->date . '</td><td scope="col">'. dayEngToMalay(date('l',strtotime($value->date))) .'</td><td scope="col">'. $i . '</td><td scope="col">' . $value->imsak . '</td><td scope="col">' . $value->maghrib . "</td></tr>";
          $i++;
      endforeach;
      $content .= '</tbody></table></div></div>';

  }

  return $content;
}
add_shortcode('waktu_solat_ramadhan', 'waktu_solat_ramadhan_shortcode');
}
add_action('init', 'waktu_solat_ramadhan_init');

// only on the front-end
if(!is_admin()) {
    function add_asyncdefer_attribute($tag, $handle) {
        // if the unique handle/name of the registered script has 'async' in it
        if (strpos($handle, 'async') !== false) {
            // return the tag with the async attribute
            return str_replace( '<script ', '<script async ', $tag );
        }
        // if the unique handle/name of the registered script has 'defer' in it
        // else if (strpos($handle, 'defer') !== false) {
        //     // return the tag with the defer attribute
        //     return str_replace( '<script ', '<script defer ', $tag );
        // }
        // otherwise skip
        else {
            return $tag;
        }
    }
    add_filter('script_loader_tag', 'add_asyncdefer_attribute', 10, 2);
}


function waktu_solat_today_big_init()
{   


    function waktu_solat_today_big_shortcode($atts = [], $content = null)
    { 
      if (isset($_GET['code'])) {
        $code = $_GET['code'];
    } else {
        $code = "sgr03";
    }


    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.azanpro.com/times/today.json?zone=".$code."&format=12-hour",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache",
          "postman-token: 2423f06b-59cf-3ec5-080c-7f66ed45af09"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        $content =  "cURL Error #:" . $err;
    } else {
        $response = json_decode($response);

        $content = '<div class="row"><div class="col-12 today">'. $response->prayer_times->date . ', '. dayEngToMalay(date('l',strtotime($response->prayer_times->date))) .'<br/>Lokasi: '.implode (", ", $response->locations).'</div></div>
        <div class="row current-day bg-light">
        <div class="col-md text-center">
        <h2>Imsak</h2>
        <h5>' . $response->prayer_times->imsak . '</h5>
        </div>
        <div class="col-md text-center">
        <h2>Subuh</h2>
        <h5>' . $response->prayer_times->subuh . '</h5>
        </div>
        <div class="col-md text-center">
        <h2>Syuruk</h2>
        <h5>' . $response->prayer_times->syuruk . '</h5>
        </div>
        <div class="col-md text-center">
        <h2>Zuhur</h2>
        <h5>' . $response->prayer_times->zohor . '</h5>
        </div>
        <div class="col-md text-center">
        <h2>Asar</h2>
        <h5>' . $response->prayer_times->asar . '</h5>
        </div>
        <div class="col-md text-center">
        <h2>Maghrib</h2>
        <h5>' . $response->prayer_times->maghrib . '</h5>
        </div>
        <div class="col-md text-center">
        <h2>Isyak</h2>
        <h5>' . $response->prayer_times->isyak . '</h5>
        </div>
        </div>';
    }

    return $content;
}
add_shortcode('waktu_solat_today_big', 'waktu_solat_today_big_shortcode');
}
add_action('init', 'waktu_solat_today_big_init');


