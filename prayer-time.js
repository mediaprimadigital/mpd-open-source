function convertTime(thetime){
  var time_to_show = thetime; // unix timestamp in seconds
  var t = new Date(time_to_show * 1000);

  hours = t.getHours();
  var hours = ((hours + 11) % 12 + 1);
  //var formatted = ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2);
  var formatted = hours + ':' + ('0' + t.getMinutes()).slice(-2);

  hours = t.getHours();
  var suffix = hours >= 12 ? "pm":"am"; hours = ((hours + 11) % 12 + 1) + suffix; 
  return(formatted+suffix);
}

function api_validate(){
  jQuery.ajax({
    type: 'GET',
    url: 'https://mpt.i906.my/mpt.json?code=wlp-0&filter=1',
    dataType: 'json',
    success: function(result){
      jQuery('.hider').show();
    },error: function (jqXHR, exception) {
      jQuery('.hider').hide();
    }
  });   
}

jQuery(document).ready(function(){
  jQuery('.hider').parent('div').show();

  api_validate();

  var d = new Date();
  var weekday = new Array(7);
  weekday[0] =  "Sunday";
  weekday[1] = "Monday";
  weekday[2] = "Tuesday";
  weekday[3] = "Wednesday";
  weekday[4] = "Thursday";
  weekday[5] = "Friday";
  weekday[6] = "Saturday";

  var month = new Array(12);
  month[0] = "January";
  month[1] = "February";
  month[2] = "March";
  month[3] = "April";
  month[4] = "May";
  month[5] = "June";
  month[6] = "July";
  month[7] = "August";
  month[8] = "September";
  month[9] = "October";
  month[10] = "November";
  month[11] = "December";

  var n = weekday[d.getDay()];

  jQuery(".today").text(d.getDate() + " " + month[d.getMonth()] + " " + d.getFullYear() +", " + n);

  //initiate dropdown select for states
  jQuery('#areaSelect').select2({
    templateSelection: formatState
  });

  function formatState (state) {
    jQuery(".today_kl_prayer").prepend("<div class='preloader' style='background-image:url(https://www.raudhah-app.my/assets/img/5.gif);background-repeat:no-repeat;background-position:center;background-color:rgba(0,0,0,.5);width:100%;height:100%;position:absolute;z-index:9999;top:0;left:0;'></div>");

    jQuery(".selectedArea").text(state.text);
    runAjax(state.id,"1");
    return state.text;
  };

  //run KL prayer time on page load
  //runAjax("wlp-2","3");

  //run KL prayer time at sidebar
  //runAjax("wlp-2","1");


  function runAjax(code,filter){
    jQuery.ajax({
      type: 'GET',
      url: 'https://mpt.i906.my/mpt.json?code='+code+'&filter='+filter,
      dataType: 'json',
      success: function(result){
        //console.log(result.response.times);
        //console.log(result.response.times[0]);

        //display today's prayer time
        //console.log(result.response.times[0]);
        if(filter!="1"){
          jQuery.each(result.response.times[d.getDate()],function(index,value){
            jQuery(".current-day>div:nth("+index+")>h5").text(convertTime(value));
          });

          jQuery(".daysPrayer").html('');
          //display the current month prayer time
          jQuery.each(result.response.times,function(index,value){
            dateme = index + 1;
            jQuery(".daysPrayer").append('<tr><th scope="row">'+ dateme + ' ' + month[d.getMonth()] + ' ' + d.getFullYear()+'</th><td>'+convertTime(value[0])+'</td><td>'+convertTime(value[1])+'</td><td>'+convertTime(value[2])+'</td><td>'+convertTime(value[3])+'</td><td>'+convertTime(value[4])+'</td><td>'+convertTime(value[5])+'</td></tr>')
            //console.log(value);
          });
          jQuery(".preloader").remove();

        }
        else{
          // console.log(result.response.times);
          //render time for KL prayer time at sidebar
          jQuery.each(result.response.times,function(index,value){
            dateme = index + 1;
            jQuery(".today_kl_prayer .kl_time:nth("+index+")").text(convertTime(value));
            console.log(index + ":" + value);
            jQuery(".preloader").remove();
          });
        }

      },error: function (jqXHR, exception) {
        jQuery('.hider').parent('div').hide();
      }
    });    
  }

});